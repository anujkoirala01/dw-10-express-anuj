// Non Primitive : Array, Object, Set, Date, Error, Null

let ar = [1, 2];
let type = typeof ar;
console.log(type);

let a = new Error("This is my error.");
console.log(typeof a);

// It means typeof non primitive is 'Object'.
