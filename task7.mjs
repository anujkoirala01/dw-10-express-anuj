// [1,2,1,["a","b"],["a","b"],["a","b"]] --> [1,2,["a","b"]]
let ar = [1, 2, 1, ["a"], ["a"]];
// map method

let arStringify = ar.map((value, i) => {
  return JSON.stringify(value);
});

console.log(arStringify);

let arSet = [...new Set(arStringify)];

console.log(arSet);

let arParse = arSet.map((value, i) => {
  return JSON.parse(value);
});

console.log(arParse)
