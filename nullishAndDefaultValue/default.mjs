// || --> if the value is truthy it takes it, but if the value is falsy it looks at another value
let b = ''
let a = b || 0 || null || 'Anuj'
console.log(a)

// || --> used as default value

// If all are falsy it takes the last value