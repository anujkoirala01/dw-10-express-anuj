let removeDuplicate = (ar)=>{
    let arStringify = ar.map((value, i) => {
        return JSON.stringify(value);
      });
      
      let arSet = [...new Set(arStringify)];
      
      let arParse = arSet.map((value, i) => {
        return JSON.parse(value);
      });

      return arParse

}

let output = removeDuplicate([1,2,1,["a"],["a"]])
console.log(output)