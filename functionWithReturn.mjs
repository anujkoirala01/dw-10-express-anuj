// function without return --> call without storing in variable
// function with return --> call with storing in variable
let sum = function()
{
    console.log("Hello")
    return 3
    // The code after return does not execute.
    console.log("I am function.")   
}

console.log("I am main.")
let s=sum()
console.log(s)

