// Set removes duplicate primitive data only i.e cannot remove duplicate non-primitives 
// But is invalid in case of 'Null' i.e it removes duplicate 'null'

let unique = [...new Set([1,2,1,{name:'Anuj'},{name:'Anuj'}, null,null])]
console.log(unique)

  