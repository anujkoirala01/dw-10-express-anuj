let ar = ['mern',120,false]

// get whole array
console.log(ar)

// get specific element
console.log(ar[0])
console.log(ar[1])
console.log(ar[2])

// change array elements

ar[1] = 140

console.log(ar[1])

// delete specific element of array

delete ar[1]  // Does not change the index of following elements
// [ 'mern', <empty item>, false]

console.log(ar)