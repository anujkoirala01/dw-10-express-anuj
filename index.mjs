import { sum } from "./task1.mjs";
import { avg } from "./task2.mjs";
import { product } from "./task3.mjs";

// task 1

// let __sum = sum(1, 0);
// console.log("The sum is =", __sum);

// task 2

// let __avg = avg(1, 2);
// console.log("The average is =", __avg);

// task 3

let __product = product(1, 2);
console.log("The product is =", __product);
