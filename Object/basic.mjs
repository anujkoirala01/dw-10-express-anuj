// obj: key + value = property

let obj = {
    name: 'mern',
    age: '120',
    isHard: false
}

// get whole object
console.log(obj)

// get specific element in object

console.log(obj.name)
console.log(obj.age)
console.log(obj.isHard)

// change specific element in object

obj.name = "MERN Stack"

console.log(obj.name)
console.log(obj)

// delete specific element

delete obj.age
console.log(obj)

