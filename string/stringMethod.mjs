let name='  Mern Stack  '

// let upperName = name.toUpperCase()
// console.log(upperName)

// let lowerName = name.toLowerCase()
// console.log(lowerName)

// let nameLength = name.length
// console.log(nameLength)

// let nameTrim = name.trim()
// console.log(nameTrim)

// let nameTrimStart = name.trimStart()
// console.log(nameTrimStart)

// let nameTrimEnd = name.trimEnd()
// console.log(nameTrimEnd)

let des = 'my name is'

let startsWithMy = des.startsWith('my')
console.log(startsWithMy)

let endsWithIs = des.endsWith('  is')
console.log(endsWithIs)

let hasName = des.includes('name')
console.log(hasName)

let replacedWith = des.replaceAll('m','mm')
console.log(replacedWith)
