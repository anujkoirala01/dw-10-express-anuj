// ?. --> optional chaining
// output is either data or undefined

let info = 
{
    name: 'Anuj',
    father:{
        age: 48
    }
}

console.log(info)

let info1 = {
    name: 'mern',
    // father :{
    //     age: 100
    // }
}

console.log(info1?.father?.age)

